# Disconnected
Disconnected is an alternative to Tmuxinator that uses a simpler structure, and uses json instead of yaml. 
If you are looking for more advanced configuration, Tmuxinator is a better option.
